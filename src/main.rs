// тут какая-то шапка

#[allow(dead_code)]
struct PowerSocket {
    desc: String,
    voltage: u16,
    amperage: u16,
    online: bool,
}

impl PowerSocket {
    fn print_help(&self) {
        todo!();
    }

    fn on(&self) {
        todo!();
    }

    fn off(&self) {
        todo!();
    }

    fn power(&self) -> u32 {
        todo!()
    }
}

#[allow(dead_code)]
struct Thermometer {
    desc: String,
    temperature: i16,
}

impl Thermometer {
    fn print_help(&self) {
        todo!();
    }

    fn temperature(&self) -> i16 {
        todo!()
    }
}

fn main() {
    let ps = PowerSocket {
        desc: String::from("Тут какое-то описание"),
        voltage: 220,
        amperage: 10,
        online: true,
    };

    ps.print_help();
    ps.on();
    ps.off();
    let _ = ps.power();

    let temp = Thermometer {
        desc: String::from("Тут еще какое-то описание"),
        temperature: 35,
    };

    temp.print_help();
    let _ = temp.temperature();
}
